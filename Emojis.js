// Definition Emoji 
class Emojis{
	constructor (emojis,vitesse = {x:30*Math.random()-15, y:30*Math.random()-15},  x=0, y=0) {
		/*constructeur, méthode appelée à la création*/
		this.emojis = emojis; 
		this.position= {};
		this.position.x= x ;
		this.position.y= y;
		this.vitesse = vitesse;
		this.nouvelle_vitesse();

		this.element = document.createElement('span');
		this.element.classList.add('emoji');
 		this.set_mode();
		this.deplacer(); 

		document.body.appendChild(this.element);

	}
	
	nouvelle_vitesse (){
		
		if (Math.abs(this.vitesse.x)+Math.abs(this.vitesse.y)<15) {
			this.vitesse = {x:10*Math.random()-5, y:10*Math.random()-5};
			this.nouvelle_vitesse;
		}
	}


	set_mode (mode = 'nature'){
		 this.element.innerHTML = this.emojis[mode];
	}

	groupe(){
		/*
			distance en x = valeur absolue (Math.abs) de x1 - x2
			meme chose en y
			nouvel x = x actuel moins distance divise par deux
			idem en y 
		*/
		var distancex = this.position.x - env.cible.x;		
		var distancey = this.position.y - env.cible.y;
		console.log(distancex);
		this.position.x=this.position.x-distancex/2+30*Math.random();
        this.position.y=this.position.y-distancey/2+30*Math.random();

		//console.log(this.position.x,env.cible.x,Math.abs(this.position.x - env.cible.x));
		//this.position=env.cible;
		this.deplacer();
	}

	deplacer (){
		this.element.style.left = this.position.x + "px"; 
		this.element.style.top = this.position.y + "px";
		
	}
	
	

	animer(){
		if (!env.clic) {
			this.rebond();

		} else {
			this.groupe();
		}


	}

	rebond(){
		//console.log("---");
		//console.log('this.position.y',this.position.y,'this.vitesse.y',this.vitesse.y);
		this.position.x += this.vitesse.x;
		this.position.y += this.vitesse.y;
		//console.log('this.position.y',this.position.y);
		
	    //BOUNCE BAS
	    if (this.position.y > window.innerHeight - this.element.offsetHeight) {
	    	this.position.y = window.innerHeight - this.element.offsetHeight;
	    	this.vitesse.y = this.vitesse.y * -1; 
	    }
		else if (this.position.y < 0) {//BOUNCE HAUT
			this.position.y = 0;
			this.vitesse.y = this.vitesse.y * -1; 
		}

		//BOUNCE DROITE
		if (this.position.x > window.innerWidth - this.element.offsetWidth) {
			this.position.x = window.innerWidth - this.element.offsetWidth;
			this.vitesse.x = this.vitesse.x * -1; 
		}
		else if (this.position.x < 0) {//BOUNCE GAUCHE
				this.position.x = 0;
				this.vitesse.x = this.vitesse.x * -1; 
		}

		this.deplacer();
	}
}

