// on créé une référence à l'interface AudioContext selon ce qui est disponible (si window.AudioContext n'est pas disponible on essaye avec webkitAudioContext)
var AudioContext = window.AudioContext || window.webkitAudioContext;

// on créé un objet contexte qui nous permet de nous connecter au système audio de l'ordinateur, et de lui envoyer du son
var contexte_audio = new AudioContext();

// dictionnaire de samples
var samples = {};

function charger_un_sample ( url, nom ) {
  /* charge un sample dont l'URL est donnée en paramètre 1 et le place dans l'objet instrument à l'emplacement de la clé donnée
  String, Function -> Void */

  // on créé l'objet requête
  var requete = new XMLHttpRequest();

  // que l'on paramètre :

  // 1 : requête GET, url de la requête, requête asynchrone : true
  requete.open('GET', url, true);

  // 2 : type de réponse
  requete.responseType = 'arraybuffer';

  // 3 : écouteur onload et fonction a exécuter alors
  requete.onload = function () {

    // les données audio
    var donnees_audio = requete.response;

    // sont passées pour décodage au contexte audio
    contexte_audio.decodeAudioData( donnees_audio, function( buffer ) {

      // on ajoute le buffer à notre objet samples
      samples[nom] = buffer;
    });
  };

  // on envoie la requête
  requete.send();
}

function jouer_sample ( nom ) {
   /* joue un sample donné */

   // on créé un BufferSource
   var player = contexte_audio.createBufferSource();

   // on récupère un buffer déjà chargé
   player.buffer = samples[ nom ];

   // on paramètre pour éviter la boucle
   player.loop = false;

   // on le connecte au contexte audio
   player.connect(contexte_audio.destination);

   // on le lance
   player.start();
}

charger_un_sample( 'sounds/click_sound.mp3', 'son_click');



document.addEventListener("click", function(evenement) {

console.log( evenement );

   // si l'accès à l'audio était suspendu
   if ( contexte_audio.state === 'suspended' ) {

      // on lève cette suspension
      contexte_audio.resume();
   }
   jouer_sample( 'son_click' );


}, false); 



